create database if not exists tradehistory;

use tradehistory;

CREATE TABLE if not exists trade_details( id int primary key auto_increment, ticker varchar(10), price double, volume int, 
	buy_or_sell varchar(5), status_code int default 0, trade_time datetime);