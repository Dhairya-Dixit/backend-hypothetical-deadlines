package com.example.demo.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Req;
import com.example.demo.entities.Trade;
import com.example.demo.repository.TradeRepository;

@Service
public class TradeService {
	
	@Autowired
	private TradeRepository traderepo;
	
	public List<Trade> getAllHistory()
	{
		List<Trade> hist = new ArrayList<Trade>();  
		traderepo.findAll().forEach(hist1 -> hist.add(hist1));
		if(hist.isEmpty())
		{
			Trade temp1 = new Trade("AAPL", 398.24, 50, Req.BUY, 0, LocalDateTime.now());
			traderepo.save(temp1);
			Trade temp2 = new Trade("GOOG", 400, 32, Req.SELL, 0, LocalDateTime.now());
			traderepo.save(temp2);
			Trade temp3 = new Trade("AMZN", 251.67, 12, Req.BUY, 0, LocalDateTime.now());
			traderepo.save(temp3);
			
			List<Trade> neohist = new ArrayList<Trade>();  
			traderepo.findAll().forEach(neohist1 -> neohist.add(neohist1));
			return neohist;
		}
		return hist;
	}
	
	
	public Trade getHistById(int id)   
	{  
		return traderepo.findById(id).get();  
	}
	
	
	public List<Trade> getHistByTicker(String ticker)
	{
		return traderepo.findByTicker(ticker);
	}
	
	
	public void addNewHist(Trade tradeslip)   
	{  
		tradeslip.setTrade_time(LocalDateTime.now());
		traderepo.save(tradeslip); 
	} 
	
	
	public void deleteHistById(int id)   
	{  
		traderepo.deleteById(id);  
	}  

	
	public int updateHistById(Trade tradeslip, int id)   
	{  
		if(traderepo.findById(id).isPresent())
		{
			Trade temps = traderepo.findById(id).get();
			LocalDateTime timey = temps.getTrade_time();
			tradeslip.setId(id);
			tradeslip.setTrade_time(timey);
			traderepo.save(tradeslip); 
			return 1;
		}
		return 0;
	}  
	
}
