package com.example.demo.entities;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name = "trade_details")
public class Trade {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String ticker;
	private double price;
	private int volume;
	@Enumerated(EnumType.STRING)
	private Req buy_or_sell;
	private int status_code = 0;
//	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime trade_time;
	
	
	public Trade() {
		
	}
	
	public Trade(String ticker, double price, int volume, Req buy_or_sell, int status_code,
			LocalDateTime trade_time) {
		super();
		this.ticker = ticker;
		this.price = price;
		this.volume = volume;
		this.buy_or_sell = buy_or_sell;
		this.status_code = status_code;
		this.trade_time = trade_time;
	}

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	

	public String getTicker() {
		return ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	
	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	
	public Req getBuy_or_sell() {
		return buy_or_sell;
	}

	public void setBuy_or_sell(Req buy_or_sell) {
		this.buy_or_sell = buy_or_sell;
	}

	
	public int getStatus_code() {
		return status_code;
	}

	public void setStatus_code(int status_code) {
		this.status_code = status_code;
	}

	
	public LocalDateTime getTrade_time() {
		return trade_time;
	}

	public void setTrade_time(LocalDateTime trade_time) {
		this.trade_time = trade_time;
	}
	
}
