package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entities.Trade;

public interface TradeRepository extends JpaRepository<Trade, Integer>{
	
	List<Trade> findByTicker(String ticker);

}
