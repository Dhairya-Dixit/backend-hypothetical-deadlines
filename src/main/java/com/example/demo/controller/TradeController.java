package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Trade;
import com.example.demo.service.TradeService;

@RestController
@RequestMapping("api/tradehistory")
public class TradeController {
	
	@Autowired
	private TradeService tradeserv;
	
	@GetMapping("/")
	public List<Trade> getAllHistory()
	{
		return tradeserv.getAllHistory();
	}
	
	
	@GetMapping("/{slipid}")
	public Trade getHistById(@PathVariable("slipid") int slipid)
	{
		return tradeserv.getHistById(slipid);
	}
	
	
	@GetMapping("/ticker/{slipticker}")
	public List<Trade> getHistByTicker(@PathVariable("slipticker") String slipticker)
	{
		return tradeserv.getHistByTicker(slipticker);
	}
	
	
	@PostMapping("/")
	public String addNewHist(@RequestBody Trade tradeslip)
	{
		tradeserv.addNewHist(tradeslip);
		return "Successfully added new Trade Slip";
	}
	
	
	@DeleteMapping("/{slipid}")
	public void deleteHistById(@PathVariable("slipid") int slipid)
	{
		tradeserv.deleteHistById(slipid);
	}
	
	@PutMapping("/{slipid}")
	public String updateHistById(@RequestBody Trade tradeslip, @PathVariable("slipid") int slipid)
	{
		int a = tradeserv.updateHistById(tradeslip, slipid);
		if(a==1)
			return "Successfully updated the Trade Slip";
		else
			return "Invalid ID";
	}

}
